<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    /*public function tryToTest(ApiTester $I)
    {
    }*/

    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/API.php/', [
            "task_title" => "AutoTaskTest",
            "task_name" => "AutoTaskName",
            "time" => "2024-06-28 08:37:20",
            "status" => "InProgress"
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldUpdateData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/API.php/1', [
            "id" => 1,
            "task_title" => "TaskUpdateSuccess",
            "task_name" => "SuccessfullyUpdated",
            "time" => "2024-06-28 08:37:20",
            "status" => "InProgress"
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldGetData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/API.php/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldDeleteData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/API.php/10/11', [10, 11]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }
}
