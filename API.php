<?php

/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");


/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");



/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");


require_once('MysqliDb.php');

class API
{
    private $db;

    public function __construct()
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'ojt_todo');
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
        // if payload is not an array
        if (!is_array($payload)) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Payload must be an array.'
            ));
            return;
        }

        // retrieve the results
        $result = $this->db->get('tbl_to_do_List');

        // if query was successful
        if ($result) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Failed Fetch Request.'
            ));
        }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        // if payload is invalid
        if (!is_array($payload) || empty($payload)) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Payload must be a non-empty array.'
            ));
            return;
        }

        // Insert payload if valid
        $result = $this->db->insert("tbl_to_do_List", array(
            "task_title" => $payload['task_title'],
            "task_name" => $payload["task_name"],
            "time" => $payload['time'],
            "status" => isset($payload['status']) ? $payload['status'] : 'Inprogress'
        ));

        // if the query was successful
        if ($result) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'success',
                'data' => $payload
            ));
        } else {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Failed to insert Data.'
            ));
        }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        // if id is null or empty
        if (empty($id)) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'ID must not be null/empty.'
            ));
            return;
        }

        // if payload is empty
        if (empty($payload)) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Payload must not be empty.',
            ));
            return;
        }

        // Making sure passed id and payload's id match
        if ($id != $payload['id']) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'ID in payload does not match provided ID.'
            ));
            return;
        }

        // Update query
        $this->db->where('id', $id);
        $result = $this->db->update('tbl_to_do_List', $payload);

        // if query was successful
        if ($result) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'success',
                'data' => $payload,
            ));
        } else {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Failed to Update Data.',
            ));
        }
    }


    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        // if id is null or empty
        if (empty($id)) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'ID must not be null/empty.'
            ));
            return;
        }

        // if payload is empty
        if (empty($payload)) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Payload must not be empty.'
            ));
            return;
        }

        // if id passed matches id in payload
        if (is_array($payload)) {
            // if many ids match many payload ids
            if ($id != $payload) {
                echo json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'IDs in payload does not match provided IDs.'
                ));
                return;
            }

            // delete multiple records
            $this->db->where('id', $payload, 'IN');
        } else {
            // id matches payload?
            if ($id != $payload) {
                echo json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'ID in payload does not match provided ID.'
                ));
                return;
            }

            // delete one record
            $this->db->where('id', $id);
        }

        // execute query
        $query = $this->db->delete('tbl_to_do_List');

        // if query was successful
        if ($query) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'data' => $id
            ));
        } else {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Failed to Delete Data'
            ));
        }
    }
}

//Identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];

// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];

        $exploded_request_uri = explode("/", $request_uri);

        // multiple idsd
        $ids = array_values(array_filter(array_map('intval', $exploded_request_uri), function ($id) {
            return $id > 0;
        }));

        // single id, probably unnecessary
        if (count($ids) == 1) {
            $ids = $ids[0];
        }
    }

    //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);
}





$api = new API;


//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}
